from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView

from apps.apis.serializers import CV_UserSerializer
from rest_framework.response import Response
from rest_framework import status


class RegistrarUsuarioApp(APIView):
    """
        Funcion que permite registrar un usuario para el manejo de tokens y autenticacion con las apis
    """
    #permission_classes = (permissions.IsAuthenticatedOrWriteOnly,)
    serializer_class = CV_UserSerializer

    def post(self, request, format=None):
        serializer = CV_UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

registrarUsuarioApis = RegistrarUsuarioApp.as_view()

from django.db import models
from django.utils import timezone

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
# Create your models here.
class PersonalizadoBaseUserManager(BaseUserManager):

    def create_user(self, usuario, password, nombres, apellidos):
        user = self.model(usuario=usuario, nombres=nombres, apellidos=apellidos)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, usuario, password, nombres, apellidos):
        user = self.create_user(usuario, password, nombres, apellidos)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Usuarios(AbstractBaseUser, PermissionsMixin):
    usuario = models.CharField(max_length=50, unique=True)
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    fecha_registro = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'usuario'

    objects = PersonalizadoBaseUserManager()

    def get_full_name(self):
        return self.nombres + self.apellidos

    def get_short_name(self):
        return self.usuario
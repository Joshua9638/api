from rest_framework.serializers import ModelSerializer
from apps.apis.models import Usuarios

class CV_UserSerializer(ModelSerializer):
    class Meta:
        model = Usuarios
        fields = ('usuario', 'password','nombres', 'apellidos', 'is_active','is_staff',)

    def create(self, validated_data):
        # create user
        isStaff = validated_data['is_staff']

        if isStaff:
            user = Usuarios.objects.db_manager('datos_gye').create_superuser(
                usuario=validated_data['usuario'],
                password=validated_data['password'],
                nombres=validated_data['nombres'],
                apellidos=validated_data['apellidos'],
            )
        else:
            user = Usuarios.objects.db_manager('datos_gye').create_user(
                usuario=validated_data['usuario'],
                password=validated_data['password'],
                nombres=validated_data['nombres'],
                apellidos=validated_data['apellidos'],
            )

        return user
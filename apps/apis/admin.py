from django.contrib import admin

# Register your models here.
from django.contrib.auth.admin import UserAdmin
from apps.apis.models import Usuarios


class PersonalizadoUserAdmin(UserAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None, {
            'fields': ('usuario', 'password1', 'password2'),
        }),
    )
    list_display = ('usuario', 'is_active', 'is_staff',)
    search_fields = ('usuario',)
    ordering = ('usuario',)


admin.site.register(Usuarios, PersonalizadoUserAdmin)
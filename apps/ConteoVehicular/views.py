from typing import Any, Callable

from django.db import connections
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UserSerializer, ConteoSerializer, UserDetailSerializer
from apps.ConteoVehicular.models import CV_Usuario, CV_ConteoVehicular
from rest_framework import status, viewsets

#-----------------------------------------------------------#
#--------- Metodos POST ------------------------------------#
#-----------------------------------------------------------#
class RegistroUsuarios(APIView):
    """
      Permite el registro de usuarios en la base Rutas GPS
    """
    serializer_class = UserSerializer

    def post(self, request, format=None):
        registroUsuario = self.serializer_class(data=request.data)

        if registroUsuario.is_valid():
            usuario = request.data['usuario']
            if existeUsuario(usuario):
                return Response(status=status.HTTP_202_ACCEPTED)# 202 indicara que el usuario ya existe por ende no se lo crea
            else:
                registroUsuario.save()
                return Response(registroUsuario.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroUsuario.errors, status=status.HTTP_400_BAD_REQUEST)

registroUsuario = RegistroUsuarios.as_view()


class RegistroVehiculos(APIView):
    """
       Permite registrar el conteo de un vehiculo
    """
    serializer_class = ConteoSerializer

    def post(self, request, format=None):
        registroConteo = self.serializer_class(data=request.data)

        if registroConteo.is_valid():
            registroConteo.save()
            return Response(registroConteo.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroConteo.errors, status=status.HTTP_400_BAD_REQUEST)

registroVehiculos = RegistroVehiculos.as_view()

#-----------------------------------------------------------#
#--------- Metodos GET ------------------------------------#
#-----------------------------------------------------------#
class ConteoVehiculosListView(generics.ListAPIView):
    """
       Funcion de vista generica basada en clase que permite obtener una lista de todos los conteos de vehiculos realizados.
       Tiene la opcion de filtrar los resultados a traves de los parametros: fecha_registro, id_usuario y tipo_vehiculo
    """
    queryset = CV_ConteoVehicular.objects.all()
    serializer_class = ConteoSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    search_fields = ('^fecha_registro',)
    filter_fields = ('id_usuario', 'tipo_vehiculo')
    ordering_fields = ('fecha_registro',)

conteoVehiculos = ConteoVehiculosListView.as_view()


class ConsultaUsuarioView(APIView):
    """
       Funcion que permite consultar la informacion del usuario. Se le pasa por parametro el nombre de usuario.
    """
    def get_object(self, usuario):
        try:
            return CV_Usuario.objects.get(usuario=usuario)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def get(self, request, usuario, format=None):
        cvUsuario = self.get_object(usuario)
        serializer = UserSerializer(cvUsuario)
        return Response(serializer.data)

consultaUsuario = ConsultaUsuarioView.as_view()

#-----------------------------------------------------------#
#--------- Metodos PUT Y DELETE --------------#
#-----------------------------------------------------------#

class ConteoVehicularDetail(APIView):
    """
       Funcion que permite realizar operaciones GET, PUT y DELETE a un registro de conteo vehicular.
       Se le pasa por parametro el id del conteo vehicular.
    """
    def get_object(self, pk):
        try:
            return CV_ConteoVehicular.objects.get(pk=pk)
        except CV_ConteoVehicular.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        conteo = self.get_object(pk)
        serializer = ConteoSerializer(conteo)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        conteo = self.get_object(pk)
        serializer = ConteoSerializer(conteo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        conteo = self.get_object(pk)
        conteo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

conteoOperaciones = ConteoVehicularDetail.as_view()


class ActualizaUsuario(APIView):
    """
       Funcion que permite actualizar la informacion de un usuario. Recibe como parametro el id del usuario
    """
    def get_object(self, pk):
        try:
            return CV_Usuario.objects.get(pk=pk)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        cvUsuario = self.get_object(pk)
        serializer = UserSerializer(cvUsuario, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

actualizaUsuario = ActualizaUsuario.as_view()

class ELiminaUsuario(APIView):
    """
        Funcion que permite eliminar un usuario. Recibe como parametro el id del usuario
    """
    def get_object(self, pk):
        try:
            return CV_Usuario.objects.get(pk=pk)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cvUsuario = self.get_object(pk)
        cvUsuario.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

eliminaUsuario = ELiminaUsuario.as_view()

def existeUsuario(usuario):
    with connections['rutas_gps'].cursor() as cursor:
        cursor.execute("select count(*) from usuario where usuario = %s", [usuario])
        rows = cursor.fetchone()

    count = rows[0]

    if count != 0:
        return True
    else:
        return False


class ListaUsuariosAppConteoVehicular(generics.ListAPIView):
    queryset = CV_Usuario.objects.filter(usuario_registro='Conteo Vehicular')
    serializer_class = UserDetailSerializer

userDetail = ListaUsuariosAppConteoVehicular.as_view()


class ELiminaUsuarioCV(APIView):
    """
        Funcion que permite eliminar un usuario. Recibe como parametro el usuario
    """
    def get_object(self, usuario):
        try:
            return CV_Usuario.objects.get(usuario=usuario)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def delete(self, request, usuario, format=None):
        cvUsuario = self.get_object(usuario)
        cvUsuario.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

eliminaUsuario3 = ELiminaUsuarioCV.as_view()

#--------- Apis con autenticacion ------------#

class Registro_Usuarios(APIView):
    """
       Permite el registro de usuarios en la base Rutas GPS
    """
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        registroUsuario = self.serializer_class(data=request.data)

        if registroUsuario.is_valid():
            usuario = request.data['usuario']
            if existeUsuario(usuario):
                return Response(status=status.HTTP_202_ACCEPTED)# 202 indicara que el usuario ya existe por ende no se lo crea
            else:
                registroUsuario.save()
                return Response(registroUsuario.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroUsuario.errors, status=status.HTTP_400_BAD_REQUEST)

registroUsuario2 = Registro_Usuarios.as_view()


class Registro_Vehiculos(APIView):
    """
        Permite registrar el conteo de un vehiculo
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ConteoSerializer

    def post(self, request, format=None):
        registroConteo = self.serializer_class(data=request.data)

        if registroConteo.is_valid():
            registroConteo.save()
            return Response(registroConteo.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroConteo.errors, status=status.HTTP_400_BAD_REQUEST)

registroVehiculos2 = Registro_Vehiculos.as_view()

#-----------------------------------------------------------#
#--------- Metodos GET ------------------------------------#
#-----------------------------------------------------------#
class Lista_Conteo_Vehiculos(generics.ListAPIView):
    """
        Funcion de vista generica basada en clase que permite obtener una lista de todos los conteos de vehiculos realizados.
        Tiene la opcion de filtrar los resultados a traves de los parametros: fecha_registro, id_usuario y tipo_vehiculo
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = CV_ConteoVehicular.objects.all()
    serializer_class = ConteoSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    search_fields = ('^fecha_registro',)
    filter_fields = ('id_usuario', 'tipo_vehiculo')
    ordering_fields = ('fecha_registro',)

conteoVehiculos2 = Lista_Conteo_Vehiculos.as_view()


class Consulta_Usuario(APIView):
    """
       Funcion que permite consultar la informacion del usuario. Se le pasa por parametro el nombre de usuario.
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, usuario):
        try:
            return CV_Usuario.objects.get(usuario=usuario)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def get(self, request, usuario, format=None):
        cvUsuario = self.get_object(usuario)
        serializer = UserSerializer(cvUsuario)
        return Response(serializer.data)

consultaUsuario2 = Consulta_Usuario.as_view()

#-----------------------------------------------------------#
#--------- Metodos PUT Y DELETE --------------#
#-----------------------------------------------------------#

class Operaciones_Conteo_Vehicular(APIView):
    """
        Funcion que permite realizar operaciones GET, PUT y DELETE a un registro de conteo vehicular.
        Se le pasa por parametro el id del conteo vehicular.
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get_object(self, pk):
        try:
            return CV_ConteoVehicular.objects.get(pk=pk)
        except CV_ConteoVehicular.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        conteo = self.get_object(pk)
        serializer = ConteoSerializer(conteo)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        conteo = self.get_object(pk)
        serializer = ConteoSerializer(conteo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        conteo = self.get_object(pk)
        conteo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

conteoOperaciones2 = Operaciones_Conteo_Vehicular.as_view()


class Actualiza_Usuario(APIView):
    """
        Funcion que permite actualizar la informacion de un usuario. Recibe como parametro el id del usuario
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk):
        try:
            return CV_Usuario.objects.get(pk=pk)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        cvUsuario = self.get_object(pk)
        serializer = UserSerializer(cvUsuario, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

actualizaUsuario2 = Actualiza_Usuario.as_view()

class ELimina_Usuario(APIView):
    """
       Funcion que permite eliminar un usuario. Recibe como parametro el id del usuario
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk):
        try:
            return CV_Usuario.objects.get(pk=pk)
        except CV_Usuario.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cvUsuario = self.get_object(pk)
        cvUsuario.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

eliminaUsuario2 = ELimina_Usuario.as_view()

def existeUsuario(usuario):
    with connections['rutas_gps'].cursor() as cursor:
        cursor.execute("select count(*) from usuario where usuario = %s", [usuario])
        rows = cursor.fetchone()

    count = rows[0]

    if count != 0:
        return True
    else:
        return False

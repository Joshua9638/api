from django.db import models

class CV_Rol(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    estado = models.CharField(max_length=1)
    identificador = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rol'
        app_label = 'rutasGPS'

class CV_Usuario(models.Model):
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    usuario = models.CharField(max_length=50)
    estado = models.CharField(max_length=1)
    usuario_registro = models.CharField(max_length=50)
    usuario_modifica = models.CharField(max_length=50, blank=True, null=True)
    fecha_modifica = models.DateTimeField(blank=True, null=True)
    rol = models.ForeignKey(CV_Rol, models.DO_NOTHING, db_column='rol', blank=True, null=True)
    clave = models.CharField(max_length=100, blank=True, null=True)
    fecha_registro = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuario'
        app_label = 'rutasGPS'


class CV_ConteoVehicular(models.Model):
    id_conteo_vehicular = models.AutoField(primary_key=True)
    latitud = models.FloatField()
    longitud = models.FloatField()
    fecha_registro = models.DateTimeField(blank=True, null=True)
    tipo_vehiculo = models.IntegerField(blank=True, null=True)
    total_conteo = models.IntegerField(blank=True, null=True)
    id_usuario = models.ForeignKey(CV_Usuario, models.DO_NOTHING, db_column='id_usuario', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'conteo_vehicular'
        app_label = 'rutasGPS'

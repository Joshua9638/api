from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer

from apps.ConteoVehicular.models import CV_Usuario, CV_ConteoVehicular


class UserSerializer(ModelSerializer):
    class Meta:
        model = CV_Usuario
        fields = ('id', 'nombres', 'apellidos', 'usuario', 'estado', 'usuario_registro', 'usuario_modifica', 'fecha_modifica', 'rol', 'clave', 'fecha_registro')


class ConteoSerializer(ModelSerializer):
    class Meta:
        model = CV_ConteoVehicular
        fields = ('id_conteo_vehicular', 'latitud', 'longitud', 'fecha_registro', 'tipo_vehiculo', 'total_conteo', 'id_usuario')


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = CV_Usuario
        fields = ('usuario',)

#from django.urls import path
from django.conf.urls import include, url

from apps.ConteoVehicular import views
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    url(r'^ConteoVehicular/registrarUsuario/$', views.registroUsuario),
    url('^ConteoVehicular/consultarUsuario/(?P<usuario>[\w.ñ@+-]+)/$', views.consultaUsuario),
    url('^ConteoVehicular/actualizarUsuario/(?P<pk>\d+)/$', views.actualizaUsuario),
    url('^ConteoVehicular/eliminarUsuario/(?P<pk>\d+)/$', views.eliminaUsuario),
    url(r'^ConteoVehicular/registrarConteoVehicular/$', views.registroVehiculos),
    url('^ConteoVehicular/listarConteoVehicular/$', views.conteoVehiculos),
    # la siguiente url realiza operaciones Retrieve, Update y Delete de un registro de conteo, se debe enviar el id
    url('^ConteoVehicular/operacionesConteo/(?P<pk>\d+)/$', views.conteoOperaciones),
    url('^ConteoVehicular/listaUsuarios/$', views.userDetail),
    url('^ConteoVehicular/eliminarUsuarioCV/(?P<usuario>[\w.ñ@+-]+)/$', views.eliminaUsuario3),
    # URLs para autenticacion
    url(r'^ConteoVehicular/registrarUsuarios/$', views.registroUsuario2),
    url('^ConteoVehicular/consultarUsuarios/(?P<usuario>[\w.ñ@+-]+)/$', views.consultaUsuario2),
    url('^ConteoVehicular/actualizarUsuarios/(?P<pk>\d+)/$', views.actualizaUsuario2),
    url('^ConteoVehicular/eliminarUsuarios/(?P<pk>\d+)/$', views.eliminaUsuario2),
    url(r'^ConteoVehicular/registraConteoVehicular/$', views.registroVehiculos2),
    url('^ConteoVehicular/listaConteoVehicular/$', views.conteoVehiculos2),
    url('^ConteoVehicular/operacionesConteoVehicular/(?P<pk>\d+)/$', views.conteoOperaciones2),



]

#urlpatterns = format_suffix_patterns(urlpatterns)

from django.urls import path
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from apps.Sectores import views


urlpatterns = [
    url('^Sectores/registroSectores/$', views.registroSectores),
    url('^Sectores/obtenerSectores/(?P<pk>[-\w]+)/$', views.obtenerSectores),
    url('^Sectores/listarSectores/$', views.listaSectores),
    url('^Sectores/modificarSectores/(?P<pk>[-\w]+)/$', views.modificarSectores),
    url('^Sectores/eliminarSectores/(?P<pk>[-\w]+)/$', views.eliminarSectores),
    url('^Sectores/RegistroTrayectoriaHist/$', views.registroTrayecHist),


    url('^Sectores/registrarSectores/$', views.registroSectores2),
    url('^Sectores/obtenerSector/(?P<pk>[-\w]+)/$', views.obtenerSectores2),
    url('^Sectores/listaSectores/$', views.listaSectores2),
    url('^Sectores/modificaSectores/(?P<pk>[-\w]+)/$', views.modificarSectores2),
    url('^Sectores/eliminaSectores/(?P<pk>[-\w]+)/$', views.eliminarSectores2),
    url('^Sectores/registrarTrayectoriaHist/$', views.registroTrayecHist2),
]


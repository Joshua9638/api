from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer

from .models import GeSectores, SecTrayectoriaGyeHist

class listaSectoresSerial(serializers.ModelSerializer):
    class Meta:
        model = GeSectores
        fields = ('uidsector','sector', 'uidciudad', 'comentario', 'coordenadas', 'puntomedio', 'estado','created_at','updated_at')

####################################################################
#               TRAYECTORIAS

class SeclistaTrayectoriaHist(serializers.ModelSerializer):
    class Meta:
        model = SecTrayectoriaGyeHist
        fields = ('id_trayectoria_hist','usuario','latitud','longitud','fecha_registro','fecha_desde','fecha_hasta','marcador_lat_desde','marcador_lng_desde','id_tipo_vehiculo','marcador_lat_hasta','marcador_lng_hasta','geom')


from typing import Any, Callable

from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView  # vistas
from .serializers import listaSectoresSerial, SeclistaTrayectoriaHist  # serializadores
from rest_framework import status, viewsets  # vistasSet
from .models import GeSectores  # Modelo
from django.http import Http404


# Ingreso de Sectores

class RegistroSectores(APIView):
    """
        Funcion que permite registrar un determinado sector
    """
    serializer_class = listaSectoresSerial

    def post(self, request, format=None):
        RegistroSectores = self.serializer_class(data=request.data)

        if RegistroSectores.is_valid():
            RegistroSectores.save()
            return Response(RegistroSectores.data, status=status.HTTP_201_CREATED)
        else:
            return Response(RegistroSectores.errors, status=status.HTTP_400_BAD_REQUEST)


registroSectores = RegistroSectores.as_view()


class ObtenerSectores(APIView):
    """
         Funcion que permite obtener Sectores
    """
    def get_object_or_404(self, pk, format=None):
        try:
            return GeSectores.objects.get(pk=pk)
        except GeSectores.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        try:
            sectores = self.get_object_or_404(pk)
            serializer = listaSectoresSerial(sectores)
            return Response(serializer.data, status=200)
        except sectores.DoesNotExist:
            raise Response(status=status.HTTP_404_NOT_FOUND)


obtenerSectores = ObtenerSectores.as_view()


class ModificarSectores(APIView):
    """
      Funcion que permite modificar Sectores
    """
    def get_object(self, pk):
        try:
            return GeSectores.objects.get(pk=pk)
        except GeSectores.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        sectores = self.get_object(pk)
        serializer = listaSectoresSerial(sectores, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


modificarSectores = ModificarSectores.as_view()


class EliminarSectores(APIView):
    """
        Funcion que permite eliminar Sectores
    """
    def get_object(self, pk):
        try:
            return GeSectores.objects.get(pk=pk)
        except GeSectores.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        GeSectores = self.get_object(pk)
        GeSectores.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


eliminarSectores = EliminarSectores.as_view()


# prueba
class ListaSectores(generics.ListAPIView):
    """
          Funcion que permite obtener lista de Sectores
    """
    queryset = GeSectores.objects.filter(estado__iexact='A').order_by('uidsector')
    serializer_class = listaSectoresSerial


listaSectores = ListaSectores.as_view()


############################################################
#                       TRAYECTORIAS
class RegistroTrayectoriasHist(APIView):
    """
          Funcion que permite registrar trayectorias historicas
    """
    serializer_class = SeclistaTrayectoriaHist

    def post(self, request, format=None):
        RegistroTrayectoriasHist = self.serializer_class(data=request.data)

        if RegistroTrayectoriasHist.is_valid():
            RegistroTrayectoriasHist.save()
            return Response(RegistroTrayectoriasHist.data, status=status.HTTP_201_CREATED)
        else:
            return Response(RegistroTrayectoriasHist.errors, status=status.HTTP_400_BAD_REQUEST)


registroTrayecHist = RegistroTrayectoriasHist.as_view()



# ---- apis con autenticacion --------------#
# Ingreso de Sectores

class Registro_Sectores(APIView):
    """
         Funcion que permite registrar un determinado sector
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = listaSectoresSerial

    def post(self, request, format=None):
        RegistroSectores = self.serializer_class(data=request.data)

        if RegistroSectores.is_valid():
            RegistroSectores.save()
            return Response(RegistroSectores.data, status=status.HTTP_201_CREATED)
        else:
            return Response(RegistroSectores.errors, status=status.HTTP_400_BAD_REQUEST)


registroSectores2 = Registro_Sectores.as_view()


class Obtener_Sectores(APIView):
    """
          Funcion que permite obtener Sectores
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get_object_or_404(self, pk, format=None):
        try:
            return GeSectores.objects.get(pk=pk)
        except GeSectores.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        try:
            sectores = self.get_object_or_404(pk)
            serializer = listaSectoresSerial(sectores)
            return Response(serializer.data, status=200)
        except sectores.DoesNotExist:
            raise Response(status=status.HTTP_404_NOT_FOUND)


obtenerSectores2 = Obtener_Sectores.as_view()


class Modificar_Sectores(APIView):
    """
        Funcion que permite modificar Sectores
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get_object(self, pk):
        try:
            return GeSectores.objects.get(pk=pk)
        except GeSectores.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        sectores = self.get_object(pk)
        serializer = listaSectoresSerial(sectores, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


modificarSectores2 = Modificar_Sectores.as_view()


class Eliminar_Sectores(APIView):
    """
         Funcion que permite eliminar Sectores
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk):
        try:
            return GeSectores.objects.get(pk=pk)
        except GeSectores.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        GeSectores = self.get_object(pk)
        GeSectores.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


eliminarSectores2 = Eliminar_Sectores.as_view()


# prueba
class Lista_Sectores(generics.ListAPIView):
    """
           Funcion que permite obtener lista de Sectores
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = GeSectores.objects.filter(estado__iexact='A').order_by('uidsector')
    serializer_class = listaSectoresSerial


listaSectores2 = Lista_Sectores.as_view()


############################################################
#                       TRAYECTORIAS
class Registro_Trayectorias_Hist(APIView):
    """
       Funcion que permite registrar trayectoria historica
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = SeclistaTrayectoriaHist

    def post(self, request, format=None):
        RegistroTrayectoriasHist = self.serializer_class(data=request.data)

        if RegistroTrayectoriasHist.is_valid():
            RegistroTrayectoriasHist.save()
            return Response(RegistroTrayectoriasHist.data, status=status.HTTP_201_CREATED)
        else:
            return Response(RegistroTrayectoriasHist.errors, status=status.HTTP_400_BAD_REQUEST)


registroTrayecHist2 = Registro_Trayectorias_Hist.as_view()

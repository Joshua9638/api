
from django.db import models

class PG_Rol(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    estado = models.CharField(max_length=1)
    identificador = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rol'
        app_label = 'rutasGPS'

class PG_Usuario(models.Model):
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    usuario = models.CharField(max_length=50)
    estado = models.CharField(max_length=1)
    usuario_registro = models.CharField(max_length=50)
    usuario_modifica = models.CharField(max_length=50, blank=True, null=True)
    fecha_modifica = models.DateTimeField(blank=True, null=True)
    rol = models.ForeignKey(PG_Rol, models.DO_NOTHING, db_column='rol', blank=True, null=True)
    clave = models.CharField(max_length=100, blank=True, null=True)
    fecha_registro = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuario'
        app_label = 'rutasGPS'

class Transporte(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'transporte'
        app_label = 'rutasGPS'


class Ruta(models.Model):
    detalle = models.TextField(blank=True, null=True)
    observacion = models.TextField(blank=True, null=True)
    fecha_registro = models.DateTimeField()
    usuario = models.ForeignKey(PG_Usuario, models.DO_NOTHING, db_column='usuario')
    usuario_elimina = models.CharField(max_length=50, blank=True, null=True)
    fecha_elimina = models.DateTimeField(blank=True, null=True)
    estado = models.CharField(max_length=1)
    origen = models.CharField(max_length=1)
    metadata = models.TextField()
    transporte = models.ForeignKey(Transporte, models.DO_NOTHING, db_column='transporte', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ruta'
        app_label = 'rutasGPS'


class DetalleRuta(models.Model):
    lon = models.CharField(max_length=25)
    lat = models.CharField(max_length=25)
    ruta = models.ForeignKey(Ruta, models.DO_NOTHING, db_column='ruta', blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)
    velocidad = models.FloatField(blank=True, null=True)
    tiempo = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'detalle_ruta'
        app_label = 'rutasGPS'

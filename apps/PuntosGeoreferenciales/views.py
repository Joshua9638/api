from typing import Any, Callable

from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import RutaSerializer, DetalleRutaSerializer, UsuarioSerializer, TransporteSerializers
from apps.PuntosGeoreferenciales.models import PG_Usuario, Ruta, DetalleRuta, Transporte
from rest_framework import status, viewsets

#-----------------------------------------------------------#
#--------- Metodos POST ------------------------------------#
#-----------------------------------------------------------#

class RegistrarUsuarios(APIView):
    serializer_class = UsuarioSerializer

    def post(self, request, format=None):
        registroUsuario = self.serializer_class(data=request.data)

        if registroUsuario.is_valid():
            registroUsuario.save()
            return Response(registroUsuario.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroUsuario.errors, status=status.HTTP_400_BAD_REQUEST)

registroUsuario = RegistrarUsuarios.as_view()


class RegistrarRuta(APIView):
    serializer_class = RutaSerializer

    def post(self, request, format=None):
        registroRuta = self.serializer_class(data=request.data)

        if registroRuta.is_valid():
            registroRuta.save()
            return Response(registroRuta.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroRuta.errors, status=status.HTTP_400_BAD_REQUEST)


registroRuta = RegistrarRuta.as_view()


class RegistrarDetalleRuta(APIView):
    serializer_class = DetalleRutaSerializer

    def post(self, request, format=None):
        registroRuta = self.serializer_class(data=request.data)

        if registroRuta.is_valid():
            registroRuta.save()
            return Response(registroRuta.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroRuta.errors, status=status.HTTP_400_BAD_REQUEST)


detalleRuta = RegistrarDetalleRuta.as_view()

#-----------------------------------------------------------#
#--------- Metodos GET ------------------------------------#
#-----------------------------------------------------------#
"""
class ConsultaUsuarioView(generics.ListAPIView):
    queryset = PG_Usuario.objects.all()
    serializer_class = UsuarioSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('usuario', 'clave', 'id')
    #filter_backends = (filters.SearchFilter,)
    #search_fields = ('=usuario', '=clave')

consultaUsuario = ConsultaUsuarioView.as_view()
"""
class ConsultaUsuarioView(APIView):
    def get_object(self, usuario):
        try:
            return PG_Usuario.objects.get(usuario=usuario)
        except PG_Usuario.DoesNotExist:
            raise Http404

    def get(self, request, usuario, format=None):
        pgUsuario = self.get_object(usuario)
        serializer = UsuarioSerializer(pgUsuario)
        return Response(serializer.data)

consultaUsuario = ConsultaUsuarioView.as_view()


class RutaListView(generics.ListAPIView):
    queryset = Ruta.objects.all()
    serializer_class = RutaSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('id', 'usuario', 'transporte')
    search_fields = ('fecha_registro',)
    ordering_fields = ('fecha_registro')
    ordering = ('fecha_registro',)


listaRuta = RutaListView.as_view()


class DetalleRutaListView(generics.ListAPIView):
    queryset = DetalleRuta.objects.all()
    serializer_class = DetalleRutaSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('ruta',)
    ordering_fields = ('orden')
    ordering = ('orden',)

listaDetalleRutaFiltrada = DetalleRutaListView.as_view()

class TransporteListView(APIView):

    def get(self, request, format=None):
        listaTransporte = Transporte.objects.all()
        serializer = TransporteSerializers(listaTransporte, many=True)
        return Response(serializer.data)

listaTransporte = TransporteListView.as_view()


class TransporteDetailView(APIView):
    def get_object(self, pk):
        try:
            return Transporte.objects.get(pk=pk)
        except Transporte.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        transporte = self.get_object(pk)
        serializer = TransporteSerializers(transporte)
        return Response(serializer.data)

transporte = TransporteDetailView.as_view()


class ActualizaUsuario(APIView):
    def get_object(self, usuario):
        try:
            return PG_Usuario.objects.get(usuario=usuario)
        except PG_Usuario.DoesNotExist:
            raise Http404

    def put(self, request, usuario, format=None):
        pgUsuario = self.get_object(usuario)
        serializer = UsuarioSerializer(pgUsuario, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

actualizaUsuario = ActualizaUsuario.as_view()

class ELiminaUsuario(APIView):
    def get_object(self, usuario):
        try:
            return PG_Usuario.objects.get(usuario=usuario)
        except PG_Usuario.DoesNotExist:
            raise Http404

    def delete(self, request, usuario, format=None):
        pgUsuario = self.get_object(usuario)
        pgUsuario.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

eliminaUsuario = ELiminaUsuario.as_view()


#----------------------- apis con autenticacion


class Registrar_Usuarios(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UsuarioSerializer

    def post(self, request, format=None):
        registroUsuario = self.serializer_class(data=request.data)

        if registroUsuario.is_valid():
            registroUsuario.save()
            return Response(registroUsuario.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroUsuario.errors, status=status.HTTP_400_BAD_REQUEST)

registroUsuario2 = Registrar_Usuarios.as_view()


class Registrar_Ruta(APIView):
    serializer_class = RutaSerializer
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request, format=None):
        registroRuta = self.serializer_class(data=request.data)

        if registroRuta.is_valid():
            registroRuta.save()
            return Response(registroRuta.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroRuta.errors, status=status.HTTP_400_BAD_REQUEST)


registroRuta2 = Registrar_Ruta.as_view()


class Registrar_DetalleRuta(APIView):
    serializer_class = DetalleRutaSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        registroRuta = self.serializer_class(data=request.data)

        if registroRuta.is_valid():
            registroRuta.save()
            return Response(registroRuta.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroRuta.errors, status=status.HTTP_400_BAD_REQUEST)


detalleRuta2 = Registrar_DetalleRuta.as_view()

#-----------------------------------------------------------#
#--------- Metodos GET ------------------------------------#
#-----------------------------------------------------------#
"""
class ConsultaUsuarioView(generics.ListAPIView):
    queryset = PG_Usuario.objects.all()
    serializer_class = UsuarioSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('usuario', 'clave', 'id')
    #filter_backends = (filters.SearchFilter,)
    #search_fields = ('=usuario', '=clave')

consultaUsuario = ConsultaUsuarioView.as_view()
"""
class Consulta_Usuario(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, usuario):
        try:
            return PG_Usuario.objects.get(usuario=usuario)
        except PG_Usuario.DoesNotExist:
            raise Http404

    def get(self, request, usuario, format=None):
        pgUsuario = self.get_object(usuario)
        serializer = UsuarioSerializer(pgUsuario)
        return Response(serializer.data)

consultaUsuario2 = Consulta_Usuario.as_view()


class Lista_rutas(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Ruta.objects.all()
    serializer_class = RutaSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('id', 'usuario', 'transporte')
    search_fields = ('fecha_registro',)
    ordering_fields = ('fecha_registro')
    ordering = ('fecha_registro',)


listaRuta2 = Lista_rutas.as_view()


class Lista_detalle_ruta(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = DetalleRuta.objects.all()
    serializer_class = DetalleRutaSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('ruta',)
    ordering_fields = ('orden')
    ordering = ('orden',)

listaDetalleRutaFiltrada2 = Lista_detalle_ruta.as_view()

class Lista_transporte(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        listaTransporte = Transporte.objects.all()
        serializer = TransporteSerializers(listaTransporte, many=True)
        return Response(serializer.data)

listaTransporte2 = Lista_transporte.as_view()


class Detalle_transporte(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Transporte.objects.get(pk=pk)
        except Transporte.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        transporte = self.get_object(pk)
        serializer = TransporteSerializers(transporte)
        return Response(serializer.data)

transporte2 = Detalle_transporte.as_view()


class Actualiza_Usuario(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, usuario):
        try:
            return PG_Usuario.objects.get(usuario=usuario)
        except PG_Usuario.DoesNotExist:
            raise Http404

    def put(self, request, usuario, format=None):
        pgUsuario = self.get_object(usuario)
        serializer = UsuarioSerializer(pgUsuario, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

actualizaUsuario2 = Actualiza_Usuario.as_view()

class Elimina_Usuario(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, usuario):
        try:
            return PG_Usuario.objects.get(usuario=usuario)
        except PG_Usuario.DoesNotExist:
            raise Http404

    def delete(self, request, usuario, format=None):
        pgUsuario = self.get_object(usuario)
        pgUsuario.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

elimina_usuario = Elimina_Usuario.as_view()

from django.urls import path
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from apps.PuntosGeoreferenciales import views

urlpatterns = [
    url(r'^PuntosGeoreferenciales/registrarRuta/$', views.registroRuta),
    url(r'^PuntosGeoreferenciales/registrarDetalleRuta/$', views.detalleRuta),
    url(r'^PuntosGeoreferenciales/listarRuta/$', views.listaRuta),
    url(r'^PuntosGeoreferenciales/listarDetalleRuta/$', views.listaDetalleRutaFiltrada),
    url(r'^PuntosGeoreferenciales/obtenerListaTransporte/$', views.listaTransporte),
    url(r'^PuntosGeoreferenciales/obtenerTransporte/(?P<pk>\d+)/$', views.transporte),
    url(r'^PuntosGeoreferenciales/registrarUsuario/$', views.registroUsuario),
    url(r'^PuntosGeoreferenciales/consultarUsuario/(?P<usuario>[-\w]+)/$', views.consultaUsuario),
    url(r'^PuntosGeoreferenciales/actualizarUsuario/(?P<usuario>[-\w]+)/$', views.actualizaUsuario),
    url(r'^PuntosGeoreferenciales/eliminarUsuario/(?P<usuario>[-\w]+)/$', views.eliminaUsuario),

    #Urls para apis con autenticacion
    url(r'^PuntosGeoreferenciales/registraRuta/$', views.registroRuta2),
    url(r'^PuntosGeoreferenciales/registraDetalleRuta/$', views.detalleRuta2),
    url(r'^PuntosGeoreferenciales/listaRuta/$', views.listaRuta2),
    url(r'^PuntosGeoreferenciales/listaDetalleRuta/$', views.listaDetalleRutaFiltrada2),
    url(r'^PuntosGeoreferenciales/obtenerListaTransportes/$', views.listaTransporte2),
    url(r'^PuntosGeoreferenciales/obtenerTransportes/(?P<pk>\d+)/$', views.transporte2),
    url(r'^PuntosGeoreferenciales/registrarUsuarios/$', views.registroUsuario2),
    url(r'^PuntosGeoreferenciales/consultarUsuarios/(?P<usuario>[-\w]+)/$', views.consultaUsuario2),
    url(r'^PuntosGeoreferenciales/actualizarUsuarios/(?P<usuario>[-\w]+)/$', views.actualizaUsuario2),
    url(r'^PuntosGeoreferenciales/eliminarUsuarios/(?P<usuario>[-\w]+)/$', views.elimina_usuario),
]

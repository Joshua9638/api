from django.apps import AppConfig


class PuntosgeoreferencialesConfig(AppConfig):
    name = 'PuntosGeoreferenciales'

from .models import *
from apps.PuntosGeoreferenciales.models import PG_Usuario, Ruta, DetalleRuta
from rest_framework.serializers import ModelSerializer

class UsuarioSerializer(ModelSerializer):
    class Meta:
        model = PG_Usuario
        fields = ('id','nombres', 'apellidos', 'usuario', 'estado', 'usuario_registro', 'usuario_modifica', 'fecha_modifica', 'rol', 'clave','fecha_registro')


class RutaSerializer(ModelSerializer):

    class Meta:
        model = Ruta
        fields = ('id', 'detalle', 'observacion', 'fecha_registro', 'usuario', 'usuario_elimina', 'fecha_elimina', 'estado', 'origen', 'metadata', 'transporte')


class DetalleRutaSerializer(ModelSerializer):
    class Meta:
        model = DetalleRuta
        fields = ('id', 'lon', 'lat', 'ruta', 'orden', 'velocidad', 'tiempo')

class TransporteSerializers(ModelSerializer):
    class Meta:
        model = Transporte
        fields = ('id', 'nombre', 'descripcion')
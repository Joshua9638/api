
from django.urls import path
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from apps.ReplicacionesRecoleccion import views

urlpatterns = [
    url(r'^Replicacion/registrarTrayectoria/$', views.registroTrayectoria),
    url(r'^Replicacion/registrarDetalleTrayectoria/$', views.detalleTrayectoria),
    #url(r'^Replicacion/registrarConteoVehicular/$', views.registroVehiculos),
    url(r'^Replicacion/crearNuevoTipoVehiculo/$', views.registroParametrosTrayectoria),
    url(r'^Replicacion/registrarUsuarioApp/$', views.registroUsuario),

    #urls para autenticacion
    url(r'^Replicacion/registraTrayectoria/$', views.registroTrayectoria2),
    url(r'^Replicacion/registraDetalleTrayectoria/$', views.detalleTrayectoria2),
    #url(r'^Replicacion/registrarConteoVehicular/$', views.registroVehiculos),
    url(r'^Replicacion/creaNuevoTipoVehiculo/$', views.registroParametrosTrayectoria2),
    url(r'^Replicacion/registraUsuarioApp/$', views.registroUsuario2),

]

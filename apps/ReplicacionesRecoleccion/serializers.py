from apps.ReplicacionesRecoleccion.models import Rep_TrayectoriaGyesDetalle, Rep_TrayectoriaGyes, Rep_TrayectoriaGyeParametros, UsuariosApp
from rest_framework.serializers import ModelSerializer

class DetalleTrayectoriaSerializer(ModelSerializer):
    class Meta:
        model = Rep_TrayectoriaGyesDetalle
        fields = ('id', 'id_trayectoria', 'longitud', 'latitud', 'orden', 'velocidad', 'tiempo', 'id_sector', 'fecha')


class TrayectoriaSerializer(ModelSerializer):
    class Meta:
        model = Rep_TrayectoriaGyes
        fields = ('id','observacion', 'fecha','id_usuario', 'estado', 'origen', 'id_transporte')

"""
class ConteoSerializer(ModelSerializer):
    class Meta:
        model = Rep_ConteoVehicular
        fields = ('id_conteo_vehicular', 'latitud', 'longitud', 'fecha_registro', 'tipo_vehiculo', 'total_conteo', 'id_usuario')

"""

class TrayectoriaGyeParametrosSerializer(ModelSerializer):
    class Meta:
        model = Rep_TrayectoriaGyeParametros
        fields = ('id_parametro', 'tipo_parametro', 'estado', 'valor_n1', 'valor_c1', 'descripcion')

class UsuariosAppSerializer(ModelSerializer):
    class Meta:
        model = UsuariosApp
        fields = ('id', 'nombres', 'apellidos', 'usuario', 'estado', 'clave', 'fecha_registro')
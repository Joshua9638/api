from django.db import models, connections


class Rep_TrayectoriaGyes(models.Model):
    id = models.IntegerField(primary_key=True)
    observacion = models.CharField(max_length=100, blank=True, null=True)
    fecha = models.DateTimeField()
    id_usuario = models.IntegerField()
    estado = models.CharField(max_length=1)
    origen = models.CharField(max_length=1)
    id_transporte = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gyes'
        app_label = 'datosGye'

class Rep_TrayectoriaGyesDetalle(models.Model):
    id_trayectoria = models.ForeignKey(Rep_TrayectoriaGyes, models.DO_NOTHING, db_column='id_trayectoria', blank=True, null=True)
    longitud = models.FloatField()
    latitud = models.FloatField()
    orden = models.IntegerField(blank=True, null=True)
    velocidad = models.FloatField(blank=True, null=True)
    tiempo = models.BigIntegerField(blank=True, null=True)
    id_sector = models.IntegerField(blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gyes_detalle'
        app_label = 'datosGye'


"""class Rep_ConteoVehicular(models.Model):
    id_conteo_vehicular = models.AutoField(primary_key=True)
    latitud = models.FloatField()
    longitud = models.FloatField()
    fecha_registro = models.DateTimeField(blank=True, null=True)
    tipo_vehiculo = models.IntegerField(blank=True, null=True)
    total_conteo = models.IntegerField(blank=True, null=True)
    id_usuario = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'conteo_vehicular'
        app_label = 'datosGye'"""

class Rep_TrayectoriaGyeParametros(models.Model):
    id_parametro = models.AutoField(primary_key=True)
    tipo_parametro = models.CharField(max_length=4000, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)
    valor_n1 = models.IntegerField(blank=True, null=True)
    valor_c1 = models.CharField(max_length=2000, blank=True, null=True)
    descripcion = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gye_parametros'
        app_label = 'datosGye'


class CoordenadasSectores(object):

    def obtenerCoordenadasSectores(self):
        with connections['datos_gye'].cursor() as cursor:
            cursor.execute("select uidsector, coordenadas from ge_sectores where estado = 'A' order by uidsector asc")
            rows = cursor.fetchall()

        return rows


class UsuariosApp(models.Model):
    id = models.IntegerField(primary_key=True)
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    usuario = models.CharField(max_length=50)
    estado = models.CharField(max_length=1)
    clave = models.CharField(max_length=1000, blank=True, null=True)
    fecha_registro = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuarios_app'
        app_label = 'datosGye'
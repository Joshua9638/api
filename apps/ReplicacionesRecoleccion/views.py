from rest_framework.views import APIView
from rest_framework.response import Response

#from apps.ReplicacionesRecoleccion.prueba3 import registrarIdSector
from .serializers import TrayectoriaSerializer, DetalleTrayectoriaSerializer, TrayectoriaGyeParametrosSerializer, UsuariosAppSerializer
from rest_framework import status, viewsets, permissions
from django.db import connections
from rest_framework.utils import json

from apps.ReplicacionesRecoleccion import models
#-----------------------------------------------------------#
#--------- Metodos POST ------------------------------------#
#-----------------------------------------------------------#

class RegistrarTrayectoria(APIView):
    """
        Funcion de replicacion que permite registrar una trayectoria
    """
    serializer_class = TrayectoriaSerializer

    def post(self, request, format=None):

        registroTrayectoria = self.serializer_class(data=request.data)


        if registroTrayectoria.is_valid():
            registroTrayectoria.save()
            return Response(registroTrayectoria.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)


registroTrayectoria = RegistrarTrayectoria.as_view()


class RegistrarDetalleTrayectoria(APIView):
    """
        Funcion de replicacion que permite registrar informacion del detalle de una trayectoria
    """
    serializer_class = DetalleTrayectoriaSerializer

    def post(self, request):
        id_trayectoria = request.data['id_trayectoria']
        #obtengo los valores del request
        lon = request.data['longitud']
        lat = request.data['latitud']
        orden = request.data['orden']
        longitud =float(lon)
        latitud = float(lat)

        registroTrayectoria = self.serializer_class(data=request.data)

        if registroTrayectoria.is_valid():
            registroTrayectoria.save()
            registrarIdSector(id_trayectoria, longitud, latitud, orden)
            return Response(registroTrayectoria.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)


detalleTrayectoria = RegistrarDetalleTrayectoria.as_view()


"""class RegistroVehiculos(APIView):
    serializer_class = ConteoSerializer

    def post(self, request, format=None):
        registroConteo = self.serializer_class(data=request.data)

        if registroConteo.is_valid():
            registroConteo.save()
            return Response(registroConteo.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroConteo.errors, status=status.HTTP_400_BAD_REQUEST)


registroVehiculos = RegistroVehiculos.as_view()
"""
class RegistrarParametroTrayectoria(APIView):
    serializer_class = TrayectoriaGyeParametrosSerializer

    def post(self, request, format=None):
        valor_c1 = request.POST['valor_c1']

        #existe = validarSiExisteTipoVehiculo(valor_c1)
        #if existe:
           #return Response(registroParametroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)
        #else:

        registroParametroTrayectoria = self.serializer_class(data=request.data)

        if registroParametroTrayectoria.is_valid():
            registroParametroTrayectoria.save()
            return Response(registroParametroTrayectoria.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroParametroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)


registroParametrosTrayectoria = RegistrarParametroTrayectoria.as_view()


#------------------------- Logica para Obtener ID Sector en base a Coordenadas ----------------------------#

#algoritmo que me indica si el punto ingresado esta dentro de un sector

def pointInPoly(x, y, poly):
    n = len(poly)
    inside = False

    p1x, p1y = poly[0]
    for i in range(n + 1):
        p2x, p2y = poly[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xints = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x, p1y = p2x, p2y

    return inside


def actualizaDetalleTrayectoria(id_trayectoria, orden, id_sector):
    with connections['datos_gye'].cursor() as cursor:
        cursor.execute("UPDATE trayectoria_gyes_detalle SET id_sector = %s WHERE id_trayectoria = %s AND orden = %s", [id_sector, id_trayectoria, orden])
        #row = cursor.fetchone()

    return 1

def registrarIdSector(id_trayectoria, longitud, latitud, orden):
    #saco lista de puntos
    id_sector = 0
    coordenadas = models.CoordenadasSectores()
    resultado = coordenadas.obtenerCoordenadasSectores()
    contador = len(resultado)
    bandera = False
    for i in range(contador):
        bandera = True
        id_sector = resultado[i][0]
        cadenaJson = resultado[i][1]
        j = json.loads(cadenaJson)
        sector = j["geometry"]["coordinates"][0]
        isTrue = pointInPoly(longitud, latitud, sector)

        if isTrue:
            actualizaDetalleTrayectoria(id_trayectoria, orden, id_sector)
            return 1
        else:
            id_sector = -1
            actualizaDetalleTrayectoria(id_trayectoria, orden, id_sector)

    if not bandera:
        id_sector = -1
        actualizaDetalleTrayectoria(id_trayectoria, orden, id_sector)


class RegistrarUsuarioApp(APIView):
    """
        Funcion de replicacion que permite registrar un usuario de la app a la base datos_gye
    """
    serializer_class = UsuariosAppSerializer

    def post(self, request, format=None):
        registroUsuarios = self.serializer_class(data=request.data)

        if registroUsuarios.is_valid():
            registroUsuarios.save()
            return Response(registroUsuarios.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroUsuarios.errors, status=status.HTTP_400_BAD_REQUEST)


registroUsuario = RegistrarUsuarioApp.as_view()


# --------------------- apis con autenticacion
class Registrar_Trayectoria(APIView):
    """
        Funcion de replicacion que permite registrar la informacion de una trayectoria
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TrayectoriaSerializer

    def post(self, request, format=None):

        registroTrayectoria = self.serializer_class(data=request.data)


        if registroTrayectoria.is_valid():
            registroTrayectoria.save()
            return Response(registroTrayectoria.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)


registroTrayectoria2 = Registrar_Trayectoria.as_view()


class Registrar_Detalle_Trayectoria(APIView):
    """
        Funcion de replicacion que permite registar el detalle de una trayectoria
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = DetalleTrayectoriaSerializer

    def post(self, request):
        id_trayectoria = request.data['id_trayectoria']
        #obtengo los valores del request
        lon = request.data['longitud']
        lat = request.data['latitud']
        orden = request.data['orden']
        #sumo -360 a la longitud para que se apegue al valor de coordenadas de los sectores
        longitud =float(lon)
        latitud = float(lat)

        registroTrayectoria = self.serializer_class(data=request.data)

        if registroTrayectoria.is_valid():
            registroTrayectoria.save()
            registrarIdSector(id_trayectoria, longitud, latitud, orden)
            return Response(registroTrayectoria.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)


detalleTrayectoria2 = Registrar_Detalle_Trayectoria.as_view()


class Registrar_Parametro_Trayectoria(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TrayectoriaGyeParametrosSerializer

    def post(self, request, format=None):
        valor_c1 = request.POST['valor_c1']

        #existe = validarSiExisteTipoVehiculo(valor_c1)
        #if existe:
           #return Response(registroParametroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)
        #else:

        registroParametroTrayectoria = self.serializer_class(data=request.data)

        if registroParametroTrayectoria.is_valid():
            registroParametroTrayectoria.save()
            return Response(registroParametroTrayectoria.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroParametroTrayectoria.errors, status=status.HTTP_400_BAD_REQUEST)


registroParametrosTrayectoria2 = Registrar_Parametro_Trayectoria.as_view()


class Registrar_Usuario_App(APIView):
    """
        Funcion de replicacion que permite registrar un usuario de la app a la base datos_gye
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = UsuariosAppSerializer

    def post(self, request, format=None):
        registroUsuarios = self.serializer_class(data=request.data)

        if registroUsuarios.is_valid():
            registroUsuarios.save()
            return Response(registroUsuarios.data, status=status.HTTP_201_CREATED)
        else:
            return Response(registroUsuarios.errors, status=status.HTTP_400_BAD_REQUEST)


registroUsuario2 = Registrar_Usuario_App.as_view()
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TblTwitter(models.Model):
    nombre = models.CharField(max_length=255)
    screen_name = models.CharField(max_length=255)
    tuits = models.TextField()
    sentimiento = models.CharField(max_length=255)
    scorepositivo = models.CharField(db_column='scorePositivo', max_length=255)  # Field name made lowercase.
    scorenegativo = models.CharField(db_column='scoreNegativo', max_length=255)  # Field name made lowercase.
    scoreneutral = models.CharField(db_column='scoreNeutral', max_length=255)  # Field name made lowercase.
    coorlatitud = models.CharField(max_length=255)
    coorlongitud = models.CharField(max_length=255)
    userlocation = models.CharField(max_length=255)
    tipogeo = models.CharField(db_column='tipoGeo', max_length=255)  # Field name made lowercase.
    coordenadageo = models.CharField(max_length=255)
    geo_enabled = models.BooleanField()
    url = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_twitter'
        app_label = 'datosGye'


class TwitterHistory(models.Model):
    data = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'twitter_history'
        app_label = 'datosGye'


class Zonas(models.Model):
    nombre = models.CharField(max_length=191, blank=True, null=True)
    coordenadas = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'zonas'
        app_label = 'datosGye'

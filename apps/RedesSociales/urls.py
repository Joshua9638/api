
from django.urls import path
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from apps.RedesSociales import views

urlpatterns = [
    url('^RedesSociales/listarTwitter/$', views.obtenerTwitter),
    url('^RedesSociales/registrarTwitter/$', views.registroTwitter),
    url('^RedesSociales/modificarTwitter/(?P<nombre>[-\w]+)/$', views.modificarTwitter),
    url('^RedesSociales/eliminarTwitter/(?P<nombre>[-\w]+)/$', views.eliminarTwitter),
    #Url - twitter tablas historicas
    url('^RedesSociales/listarTwitterHistorico/$', views.twitterHist),
    #Url - twitter tablas zonas
    url('^RedesSociales/listarZonasTwitter/$', views.zona),

    # url apis con autenticacion
    url('^RedesSociales/listaTwitter/$', views.obtenerTwitter2),
    url('^RedesSociales/registraTwitter/$', views.registroTwitter2),
    url('^RedesSociales/modificaTwitter/(?P<nombre>[-\w]+)/$', views.modificarTwitter2),
    url('^RedesSociales/eliminaTwitter/(?P<nombre>[-\w]+)/$', views.eliminarTwitter2),
    # Url - twitter tablas historicas
    url('^RedesSociales/listaTwitterHistorico/$', views.twitterHist2),
    # Url - twitter tablas zonas
    url('^RedesSociales/listaZonasTwitter/$', views.zona2),
]

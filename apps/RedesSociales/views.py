from typing import Any, Callable

from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions
from rest_framework.views import APIView #vistas
from rest_framework.response import Response
from .serializers import TwitterSerializer, TwitterHistSerializer, zonasSerializer #serializadores
from rest_framework import status, viewsets #vistasSet
from .models import TblTwitter, TwitterHistory, Zonas #Modelo

class RegistroTwitter(APIView):
    """
         Funcion que permite registrar un tweet
    """
    serializer_class = TwitterSerializer

    def post(self, request, format=None):
        RegistroTwitter = self.serializer_class(data=request.data)

        if RegistroTwitter.is_valid():
            RegistroTwitter.save()
            return Response(RegistroTwitter.data, status=status.HTTP_201_CREATED)
        else:
            return Response(RegistroTwitter.errors, status=status.HTTP_400_BAD_REQUEST)
registroTwitter =RegistroTwitter.as_view()

class ObtenerTwitter(APIView):
    """
         Funcion que permite obtener la informacion de un tweet
    """
    def get(self, request, format=None):
        try:
            twitter = TblTwitter.objects.all()
            serializer = TwitterSerializer(twitter, many=True)
            return Response(serializer.data)
        except twitter.DoesNotExist:
            raise Response(status=status.HTTP_400_BAD_REQUEST)
obtenerTwitter = ObtenerTwitter.as_view()

class ModificarTwitter(APIView):
    """
          Funcion que permite modificar la informacion de un tweet
    """
    def get_object(self, nombre):
        try:
            return TblTwitter.objects.get(nombre=nombre)
        except TblTwitter.DoesNotExist:
            raise Http404

    def put(self, request,nombre, format=None):
        twitter = self.get_object(nombre)
        serializer = TwitterSerializer(twitter, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
modificarTwitter=ModificarTwitter.as_view()

class EliminarTwitter(APIView):
    """
           Funcion que permite eliminar la informacion de un tweet
    """
    def get_object(self, nombre):
        try:
            return TblTwitter.objects.get(nombre=nombre)
        except TblTwitter.DoesNotExist:
            raise Http404

    def delete(self, request, nombre, format=None):
        TblTwitter = self.get_object(nombre)
        TblTwitter.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
eliminarTwitter=EliminarTwitter.as_view()


###################################################################################
#                   TWITTER - HISTORICA
class TwitterHistorica(generics.ListAPIView):
    """
           Funcion que presenta toda la informacion de la tabla historica de twitter
    """
    serializer_class = TwitterHistSerializer
    queryset = TwitterHistory.objects.all()

twitterHist = TwitterHistorica.as_view()



###################################################################################
#                   TWITTER - Zonas
class twitterZona(generics.ListAPIView):
    """
           Funcion que permite mostrar las zonas de twitter
    """
    queryset = Zonas.objects.all()
    serializer_class = zonasSerializer

zona= twitterZona.as_view()



#MetodosPrueba
class TwitterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TblTwitter.objects.all()
    serializer_class = TwitterSerializer

class twitterList(generics.ListCreateAPIView):
    queryset = TblTwitter.objects.all()
    serializer_class = TwitterSerializer

# ------------- Apis con autenticacion ----------#

class Registro_Twitter(APIView):
    """
          Funcion que permite registrar un tweet
    """
    serializer_class = TwitterSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        RegistroTwitter = self.serializer_class(data=request.data)

        if RegistroTwitter.is_valid():
            RegistroTwitter.save()
            return Response(RegistroTwitter.data, status=status.HTTP_201_CREATED)
        else:
            return Response(RegistroTwitter.errors, status=status.HTTP_400_BAD_REQUEST)
registroTwitter2 =Registro_Twitter.as_view()

class Obtener_Twitter(APIView):
    """
          Funcion que permite obtener la informacion de un tweet
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, format=None):
        try:
            twitter = TblTwitter.objects.all()
            serializer = TwitterSerializer(twitter, many=True)
            return Response(serializer.data)
        except twitter.DoesNotExist:
            raise Response(status=status.HTTP_400_BAD_REQUEST)
obtenerTwitter2 = Obtener_Twitter.as_view()

class Modificar_Twitter(APIView):
    """
           Funcion que permite modificar la informacion de un tweet
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get_object(self, nombre):
        try:
            return TblTwitter.objects.get(nombre=nombre)
        except TblTwitter.DoesNotExist:
            raise Http404

    def put(self, request,nombre, format=None):
        twitter = self.get_object(nombre)
        serializer = TwitterSerializer(twitter, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
modificarTwitter2=Modificar_Twitter.as_view()

class Eliminar_Twitter(APIView):
    """
            Funcion que permite eliminar la informacion de un tweet
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get_object(self, nombre):
        try:
            return TblTwitter.objects.get(nombre=nombre)
        except TblTwitter.DoesNotExist:
            raise Http404

    def delete(self, request, nombre, format=None):
        TblTwitter = self.get_object(nombre)
        TblTwitter.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
eliminarTwitter2=Eliminar_Twitter.as_view()


###################################################################################
#                   TWITTER - HISTORICA
class Twitter_Historica(generics.ListAPIView):
    """
            Funcion que presenta toda la informacion de la tabla historica de twitter
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TwitterHistSerializer
    queryset = TwitterHistory.objects.all()

twitterHist2 = Twitter_Historica.as_view()



###################################################################################
#                   TWITTER - Zonas
class twitter_Zona(generics.ListAPIView):
    """
            Funcion que permite mostrar las zonas de twitter
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Zonas.objects.all()
    serializer_class = zonasSerializer

zona2= twitter_Zona.as_view()



#MetodosPrueba
class TwitterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TblTwitter.objects.all()
    serializer_class = TwitterSerializer

class twitterList(generics.ListCreateAPIView):
    queryset = TblTwitter.objects.all()
    serializer_class = TwitterSerializer
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from .models import TblTwitter, TwitterHistory, Zonas
from rest_framework import generics 

class TwitterSerializer(serializers.ModelSerializer):
    class Meta:
        model = TblTwitter
        fields = ('nombre', 'screen_name', 'tuits', 'sentimiento', 'scorepositivo', 'scorenegativo','scoreneutral','coorlatitud','coorlongitud','userlocation','tipogeo','coordenadageo','geo_enabled','url','created_at','updated_at')

class TwitterMESerializer(HyperlinkedModelSerializer):
    nombre=TwitterSerializer(many=False, read_only=True)
    class Meta:
        model = TblTwitter
        fields = ('nombre', 'screen_name', 'tuits', 'sentimiento', 'scorepositivo', 'scorenegativo','scoreneutral','coorlatitud','coorlongitud','userlocation','tipogeo','coordenadageo','geo_enabled','url','created_at','updated_at')

##########################################################################
#               Twitter - Historica
class TwitterHistSerializer(serializers.ModelSerializer):
    class Meta:
        model=TwitterHistory
        fields =('data','created_at','updated_at')


#########################################################################
#                   Zonas 
class zonasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zonas
        fields =('nombre','coordenadas','created_at','updated_at')

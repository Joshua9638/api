from django.db import models
from django.db import connections

class SequenceNumber(object):
    def __init__(self):
        # Initialize any variables you need from the input you get
        pass

    def get_id_sequence_number(self):
        with connections['datos_gye'].cursor() as cursor:
            cursor.execute("select nextval ('trayectoria_id_seq')")
            row = cursor.fetchone()

        return row[0]

    def get_id_sequence_tipo_transporte(self):
        with connections['rutas_gps'].cursor() as cursor:
            cursor.execute("select nextval ('transporte_id_seq')")
            row = cursor.fetchone()

        return row[0]

    def get_id_sequence_conteo(self):
        with connections['datos_gye'].cursor() as cursor:
            cursor.execute("select nextval ('conteo_vehicular_id_vehiculo_seq')")
            row = cursor.fetchone()

        return row[0]


    def get_id_sequence_usuario(self):
        with connections['datos_gye'].cursor() as cursor:
            cursor.execute("select nextval ('usuario_id_seq')")
            row = cursor.fetchone()

        return row[0]
#sec_parametros


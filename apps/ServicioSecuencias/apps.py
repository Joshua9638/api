from django.apps import AppConfig


class ServiciosecuenciasConfig(AppConfig):
    name = 'ServicioSecuencias'

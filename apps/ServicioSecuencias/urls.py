from django.conf.urls import url

from apps.ServicioSecuencias import views
#from django.conf.urls.defaults import *

urlpatterns = [
    url(r'^Secuencia/obtenerIdTrayectoria/$', views.idTrayectoria),
    url(r'^Secuencia/obtenerIdConteoVehicular/$', views.idConteo),
    url(r'^Secuencia/obtenerIdTipoTransporte/$', views.idTipoTransporte),
    url(r'^Secuencia/obtenerIdUsuario/$', views.idUsuario),


    url(r'^Secuencias/obtenerIdTrayectoria/$', views.idTrayectoria2),
    url(r'^Secuencias/obtenerIdConteoVehicular/$', views.idConteo2),
    url(r'^Secuencias/obtenerIdTipoTransporte/$', views.idTipoTransporte2),
    url(r'^Secuencias/obtenerIdUsuario/$', views.idUsuario2),
]


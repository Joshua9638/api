from rest_framework.response import Response
from rest_framework import status, permissions

from rest_framework.views import APIView

#from apps.ServicioSecuencias import getSequenceNumber

from apps.ServicioSecuencias.models import SequenceNumber

class IdTrayectoria(APIView):
    """
       Funcion que retorna un entero (id_trayectoria)
    """

    def get(self, request):
        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_number()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idTrayectoria = IdTrayectoria.as_view()


class IdTipoTransporte(APIView):
    """
          Permite obtener secuencia del tipo_transporte
    """
    def get(self, request):

        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_tipo_transporte()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idTipoTransporte = IdTipoTransporte.as_view()


class IdConteoVehicular(APIView):
    """
             Permite obtener secuencia del id_conteo_vehicular
    """
    def get(self, request):

        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_conteo()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idConteo = IdConteoVehicular.as_view()


class IdUsuario(APIView):
    """
        Permite obtener secuencia del id_usuario
    """

    def get(self, request):

        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_usuario()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idUsuario = IdUsuario.as_view()


# - apis con autenticacion

class Id_Trayectoria(APIView):
    """
        Funcion que retorna un entero (id_trayectoria)
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):
        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_number()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idTrayectoria2 = Id_Trayectoria.as_view()


class Id_Tipo_Transporte(APIView):
    """
           Permite obtener secuencia del tipo_transporte
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):

        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_tipo_transporte()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idTipoTransporte2 = Id_Tipo_Transporte.as_view()


class Id_Conteo_Vehicular(APIView):
    """
         Permite obtener secuencia del id_conteo_vehicular
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):

        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_conteo()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idConteo2 = Id_Conteo_Vehicular.as_view()


class Id_Usuario(APIView):
    """
       Permite obtener secuencia del id_usuario
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):

        secuencia = SequenceNumber()
        result = secuencia.get_id_sequence_usuario()

        response = Response(result, status=status.HTTP_200_OK)

        return response

idUsuario2 = Id_Usuario.as_view()
from django.conf.urls import include, url

from apps.Indicadores import views

urlpatterns = [
    url(r'^Indicadores/obtenerSectores/$', views.geSectores),
    url(r'^Indicadores/obtenerSector/(?P<pk>\d+)/$', views.geSector),

    url(r'^Indicadores/obtenerTrayectoria/$', views.listaTrayectoria),
    url(r'^Indicadores/obtenerDetalleTrayectoria/$', views.detalleTrayectoria),

    url(r'^Indicadores/obtenerParametros/$', views.trayectoriaGyesParametros),
    url(r'^Indicadores/obtenerParametro/(?P<pk>\d+)/$', views.detalleTrayectoriaParams),

    url(r'^Indicadores/obtenerListaConteoVehicular/$', views.conteoVehiculos),
    url(r'^Indicadores/operacionesConteoVehicular/(?P<pk>\d+)/$', views.conteoOperaciones),
    url(r'^Indicadores/obtenerAnio/(?P<pk>\d+)/$', views.anio),
    url(r'^Indicadores/obtenerAnios/$', views.anios),
    url(r'^Indicadores/obtenerMes/(?P<pk>\d+)/$', views.mes),
    url(r'^Indicadores/obtenerMeses/$', views.meses),
    url(r'^Indicadores/obtenerListaTipoVehiculo/$', views.tipoVehiculos),
    url(r'^Indicadores/obtenerTipoVehiculo/(?P<pk>\d+)/$', views.tipoVehiculo),

    url(r'^Indicadores/obtenerListaRecoleccionDatos/$', views.recoleccionDatos),
    url(r'^Indicadores/obtenerRecoleccionDato/(?P<pk>\d+)/$', views.recoleccionDato),
    url(r'^Indicadores/obtenerListaTipoSector/$', views.tipoSectores),
    url(r'^Indicadores/obtenerTipoSector/(?P<pk>\d+)/$', views.tipoSector),



]

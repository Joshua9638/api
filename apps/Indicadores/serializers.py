from apps.Indicadores.models import *
from rest_framework.serializers import ModelSerializer


class DetalleTrayectoriaSerializer(ModelSerializer):
    class Meta:
        model = IndTrayectoriaGyesDetalle
        fields = ('id', 'id_trayectoria', 'orden', 'velocidad', 'id_sector', 'fecha')


class TrayectoriaSerializer(ModelSerializer):
    class Meta:
        model = IndTrayectoriaGyes
        fields = ('id','observacion', 'fecha', 'id_transporte')


class ConteoSerializer(ModelSerializer):
    class Meta:
        model = IndConteoVehicular
        fields = ('id_vehiculo', 'cant_autos', 'cant_buses', 'cant_total', 'latitud', 'longitud', 'fecha_registro', 'id_usuario')

class AnioSerializer(ModelSerializer):
    class Meta:
        model = Anio
        fields = ('id_anio', 'descripcion')


class GeSectoresSerializer(ModelSerializer):
    class Meta:
        model = IndGeSectores
        fields = ('uidsector', 'sector', 'uidciudad', 'comentario', 'coordenadas', 'puntomedio')

class MesSerializer(ModelSerializer):
    class Meta:
        model = Mes
        fields = ('id_mes', 'descripcion')

class RecoleccionDatosSerializer(ModelSerializer):
    class Meta:
        model = IndRecoleccionDatos
        fields = ('id_recoleccion', 'id_anio', 'id_mes', 'id_vehiculo', 'transporte', 'cantidad')

class TipoVehiculoSerializer(ModelSerializer):
    class Meta:
        model = IndTipoVehiculo
        fields = ('id_vehiculo', 'nombre', 'fecha')

class TipoSectorSerializer(ModelSerializer):
    class Meta:
        model = IndTipoSector
        fields = ('nombre', 'descripcion', 'fecha', 'id_sector')

class TrayectoriaGyeParametrosSerializer(ModelSerializer):
    class Meta:
        model = IndTrayectoriaGyeParametros
        fields = ('id_parametro', 'tipo_parametro', 'valor_c1')


from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from apps.Indicadores.serializers import *
from apps.Indicadores.models import *
from rest_framework import status, viewsets


# Create your views here.
# -----------------------------------------------------------#
# --------- Metodos GET ------------------------------------#
# -----------------------------------------------------------#
class TrayectoriaListView(generics.ListAPIView):
    queryset = IndTrayectoriaGyes.objects.all()
    serializer_class = TrayectoriaSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('id', 'fecha')
    search_fields = ('fecha',)
    ordering_fields = ('fecha')
    ordering = ('id', 'fecha',)


listaTrayectoria = TrayectoriaListView.as_view()


class DetalleTrayectoriaListView(generics.ListAPIView):
    queryset = IndTrayectoriaGyesDetalle.objects.all()
    serializer_class = DetalleTrayectoriaSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('id_trayectoria',)
    ordering_fields = ('orden')
    ordering = ('orden')


detalleTrayectoria = DetalleTrayectoriaListView.as_view()


class ConteoVehiculosListView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = IndConteoVehicular.objects.all()
    serializer_class = ConteoSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('fecha_registro', 'id_usuario')
    ordering_fields = ('fecha_registro')
    ordening = ('fecha_registro')


conteoVehiculos = ConteoVehiculosListView.as_view()


class ConteoVehicularDetail(APIView):

    def get_object(self, pk):
        try:
            return IndConteoVehicular.objects.get(pk=pk)
        except IndConteoVehicular.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        conteo = self.get_object(pk)
        serializer = ConteoSerializer(conteo)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        conteo = self.get_object(pk)
        serializer = ConteoSerializer(conteo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        conteo = self.get_object(pk)
        conteo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


conteoOperaciones = ConteoVehicularDetail.as_view()


class AnioDetail(APIView):

    def get_object(self, pk):
        try:
            return Anio.objects.get(pk=pk)
        except Anio.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        anio = self.get_object(pk)
        serializer = AnioSerializer(anio)
        return Response(serializer.data)


anio = AnioDetail.as_view()


class AnioList(APIView):
    def get(self, request, format=None):
        anios = Anio.objects.all()
        serializer = AnioSerializer(anios, many=True)
        return Response(serializer.data)


anios = AnioList.as_view()


class MesDetail(APIView):
    def get_object(self, pk):
        try:
            return Mes.objects.get(pk=pk)
        except Mes.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        mes = self.get_object(pk)
        serializer = MesSerializer(mes)
        return Response(serializer.data)


mes = MesDetail.as_view()


class MesList(APIView):
    def get(self, request, format=None):
        meses = Mes.objects.all()
        serializer = MesSerializer(meses, many=True)
        return Response(serializer.data)


meses = MesList.as_view()


class TipoVehiculoList(APIView):
    def get(self, request, format=None):
        tipovehiculos = IndTipoVehiculo.objects.all()
        serializer = TipoVehiculoSerializer(tipovehiculos, many=True)
        return Response(serializer.data)


tipoVehiculos = TipoVehiculoList.as_view()


class TipoVehiculoDetail(APIView):
    def get_object(self, pk):
        try:
            return IndTipoVehiculo.objects.get(pk=pk)
        except IndTipoVehiculo.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        tipoVehiculo = self.get_object(pk)
        serializer = TipoVehiculoSerializer(tipoVehiculo)
        return Response(serializer.data)


tipoVehiculo = TipoVehiculoDetail.as_view()


# ---------------SECTORES------------------------------------#
class GeSectoresList(APIView):
    def get(self, request, format=None):
        sectores = IndGeSectores.objects.all()
        serializer = GeSectoresSerializer(sectores, many=True)
        return Response(serializer.data)


geSectores = GeSectoresList.as_view()


class GeSectoresDetail(APIView):
    def get_object(self, pk):
        try:
            return IndGeSectores.objects.get(pk=pk)
        except IndGeSectores.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        sector = self.get_object(pk)
        serializer = GeSectoresSerializer(sector)
        return Response(serializer.data)


geSector = GeSectoresDetail.as_view()


# -------------------------------------------------------------#


class RecoleccionDatosList(generics.ListAPIView):
    queryset = IndRecoleccionDatos.objects.all()
    serializer_class = RecoleccionDatosSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('id_anio', 'id_mes', 'id_vehiculo', 'transporte')
    ordening = ('id_recoleccion')

recoleccionDatos = RecoleccionDatosList.as_view()

class ConteoVehiculosListView(generics.ListAPIView):
    queryset = IndConteoVehicular.objects.all()
    serializer_class = ConteoSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('fecha_registro', 'id_usuario')
    ordering_fields = ('fecha_registro')
    ordening = ('fecha_registro')

class RecoleccionDatosDetail(APIView):
    def get_object(self, pk):
        try:
            return IndRecoleccionDatos.objects.get(pk=pk)
        except IndRecoleccionDatos.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        recoleccionDato = self.get_object(pk)
        serializer = RecoleccionDatosSerializer(recoleccionDato)
        return Response(serializer.data)


recoleccionDato = RecoleccionDatosDetail.as_view()


class TipoSectorList(APIView):
    def get(self, request, format=None):
        tipoSectores = IndTipoSector.objects.all()
        serializer = TipoSectorSerializer(tipoSectores, many=True)
        return Response(serializer.data)


tipoSectores = TipoSectorList.as_view()


class TipoSectorDetail(APIView):
    def get_object(self, pk):
        try:
            return IndTipoSector.objects.get(pk=pk)
        except IndTipoSector.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        tipoSector = self.get_object(pk)
        serializer = TipoSectorSerializer(tipoSector)
        return Response(serializer.data)


tipoSector = TipoSectorDetail.as_view()


class TrayectoriaGyeParametrosList(generics.ListAPIView):
    queryset = IndTrayectoriaGyeParametros.objects.all()
    serializer_class = TrayectoriaGyeParametrosSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('tipo_parametro',)
    ordering = ('id_parametro')


trayectoriaGyesParametros = TrayectoriaGyeParametrosList.as_view()


class TrayectoriaGyeParametrosDetail(APIView):
    def get_object(self, pk):
        try:
            return IndTrayectoriaGyeParametros.objects.get(pk=pk)
        except IndTrayectoriaGyeParametros.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        trayectoriaGye = self.get_object(pk)
        serializer = TrayectoriaGyeParametrosSerializer(trayectoriaGye)
        return Response(serializer.data)


detalleTrayectoriaParams = TrayectoriaGyeParametrosDetail.as_view()

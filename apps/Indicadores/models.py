
from django.db import models


class Anio(models.Model):
    id_anio = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'anio'
        app_label = 'datosGye'

class IndTipoVehiculo(models.Model):
    id_vehiculo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=2000)
    fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipo_vehiculo'
        app_label = 'datosGye'

class IndConteoVehicular(models.Model):
    id_vehiculo = models.AutoField(primary_key=True)
    cant_autos = models.IntegerField()
    cant_buses = models.IntegerField()
    cant_total = models.IntegerField()
    latitud = models.CharField(max_length=100)
    longitud = models.CharField(max_length=100)
    fecha_registro = models.DateField(blank=True, null=True)
    id_usuario = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'conteo_vehicular'
        app_label = 'datosGye'


class IndGeSectores(models.Model):
    uidsector = models.AutoField(primary_key=True)
    sector = models.CharField(max_length=200, blank=True, null=True)
    uidciudad = models.IntegerField(blank=True, null=True)
    comentario = models.CharField(max_length=2000, blank=True, null=True)
    coordenadas = models.TextField()
    puntomedio = models.TextField()

    class Meta:
        managed = False
        db_table = 'ge_sectores'
        app_label = 'datosGye'


class Mes(models.Model):
    id_mes = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'mes'
        app_label = 'datosGye'


class IndRecoleccionDatos(models.Model):
    id_recoleccion = models.IntegerField(primary_key=True)
    id_anio = models.ForeignKey(Anio, models.DO_NOTHING, db_column='id_anio')
    id_mes = models.ForeignKey(Mes, models.DO_NOTHING, db_column='id_mes')
    id_vehiculo = models.ForeignKey(IndTipoVehiculo, models.DO_NOTHING, db_column='id_vehiculo')
    transporte = models.CharField(max_length=-1)
    cantidad = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'recoleccion_datos'
        app_label = 'datosGye'


class IndTipoSector(models.Model):
    nombre = models.CharField(max_length=2000)
    descripcion = models.CharField(max_length=2000, blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True)
    id_sector = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tipo_sector'
        app_label = 'datosGye'


class IndTrayectoriaGyeParametros(models.Model):
    id_parametro = models.AutoField(primary_key=True)
    tipo_parametro = models.CharField(max_length=4000, blank=True, null=True)
    estado = models.CharField(max_length=1, blank=True, null=True)
    valor_n1 = models.IntegerField(blank=True, null=True)
    valor_c1 = models.CharField(max_length=2000, blank=True, null=True)
    descripcion = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gye_parametros'
        app_label = 'datosGye'


class IndTrayectoriaGyes(models.Model):
    id = models.IntegerField(primary_key=True)
    observacion = models.CharField(max_length=100, blank=True, null=True)
    fecha = models.DateTimeField()
    id_usuario = models.IntegerField()
    estado = models.CharField(max_length=1)
    origen = models.CharField(max_length=1)
    id_transporte = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gyes'
        app_label = 'datosGye'


class IndTrayectoriaGyesDetalle(models.Model):
    id_trayectoria = models.ForeignKey(IndTrayectoriaGyes, models.DO_NOTHING, db_column='id_trayectoria', blank=True, null=True)
    longitud = models.FloatField()
    latitud = models.FloatField()
    orden = models.IntegerField(blank=True, null=True)
    velocidad = models.FloatField(blank=True, null=True)
    tiempo = models.BigIntegerField(blank=True, null=True)
    id_sector = models.IntegerField(blank=True, null=True)
    fecha = models.DateTimeField()
    class Meta:
        managed = False
        db_table = 'trayectoria_gyes_detalle'
        app_label = 'datosGye'

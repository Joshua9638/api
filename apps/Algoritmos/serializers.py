from rest_framework import serializers
#import datetime
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from .models import AlgTrayectoriaGyes, AlgTrayectoriaGyesDetalle,  AlgTrayectoriaGyeHist #Modelo
from django.db.models import DateTimeField
from django.db.models.fields import DateField

####################################################################
#       TRAYECTORIA GYE DETALLE

class TrayectoriaGyeSerial(serializers.ModelSerializer):
    #id_trayectoria = TrayectoriaGyeDetalleSerial(many=True, read_only=True)
    fecha = serializers.DateTimeField(format="%Y-%m-%d %H:%M", input_formats=['%Y-%m-%d %H:%M',''])
    class Meta:
        model = AlgTrayectoriaGyes
        fields = ('id','observacion','fecha','id_usuario','estado','origen','id_transporte')
        
class TrayectoriaGyeDetalleSerial(serializers.ModelSerializer):
    id_trayectoria = TrayectoriaGyeSerial(many=False, read_only=True)
    fecha = serializers.DateTimeField(format="%Y-%m-%d %H:%M", input_formats=['%Y-%m-%d %H:%M',''])
    #fecha = serializers.DateField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = AlgTrayectoriaGyesDetalle
        fields = ('id_trayectoria','id','longitud','latitud','orden','velocidad','tiempo','id_sector','fecha')
        


####################################################################
#           TRAYECTORIA HISTORICA

class TrayectoriaHistSerial(serializers.ModelSerializer):
    class Meta:
        model = AlgTrayectoriaGyeHist
        fields = ('id_trayectoria_hist','usuario','latitud','longitud','fecha_registro','fecha_desde','fecha_hasta','marcador_lat_desde','marcador_lng_desde','id_tipo_vehiculo','marcador_lat_hasta','marcador_lng_hasta','geom')

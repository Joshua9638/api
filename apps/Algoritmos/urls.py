
from django.urls import path
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from apps.Algoritmos import views

urlpatterns = [
       #PRUEBAS
       url('^Algoritmo/listarAlgoritmoGyes/$', views.trayectoria),
       #url('^Algoritmo/listarAlgoritmoGyesFecha/$', views.trayectoriafecha),
       url('^Algoritmo/listarTrayectoriaHist/$', views.trayectoriaHist),

       url('^Algoritmos/listarAlgoritmoGyes/$', views.trayectoria2),
       #url('^Algoritmo/listarAlgoritmoGyesFecha/$', views.trayectoriafecha),
       url('^Algoritmos/listarTrayectoriaHist/$', views.trayectoriaHist2),
]
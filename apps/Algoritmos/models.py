# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AlgTrayectoriaGyes(models.Model):
    id = models.IntegerField(primary_key=True)
    observacion = models.CharField(max_length=100, blank=True, null=True)
    fecha = models.DateTimeField()
    id_usuario = models.IntegerField()
    estado = models.CharField(max_length=1)
    origen = models.CharField(max_length=1)
    id_transporte = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gyes'
        app_label = 'datosGye'


class AlgTrayectoriaGyesDetalle(models.Model):
    id = models.IntegerField(primary_key=True)
    id_trayectoria = models.ForeignKey(AlgTrayectoriaGyes, models.DO_NOTHING, db_column='id_trayectoria', blank=True, null=True)
    longitud = models.FloatField()
    latitud = models.FloatField()
    orden = models.IntegerField(blank=True, null=True)
    velocidad = models.FloatField(blank=True, null=True)
    tiempo = models.BigIntegerField(blank=True, null=True)
    id_sector = models.IntegerField(blank=True, null=True)
    fecha = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trayectoria_gyes_detalle'
        app_label = 'datosGye'


class AlgTrayectoriaGyeHist(models.Model):
    id_trayectoria_hist = models.AutoField(primary_key=True)
    usuario = models.CharField(max_length=2000, blank=True, null=True)
    latitud = models.FloatField(blank=True, null=True)
    longitud = models.FloatField(blank=True, null=True)
    fecha_registro = models.DateTimeField(blank=True, null=True)
    fecha_desde = models.DateTimeField(blank=True, null=True)
    fecha_hasta = models.DateTimeField(blank=True, null=True)
    marcador_lat_desde = models.FloatField(blank=True, null=True)
    marcador_lng_desde = models.FloatField(blank=True, null=True)
    id_tipo_vehiculo = models.IntegerField(blank=True, null=True)
    marcador_lat_hasta = models.FloatField(blank=True, null=True)
    marcador_lng_hasta = models.FloatField(blank=True, null=True)
    geom = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'trayectoria_gye_hist'
        app_label = 'datosGye'

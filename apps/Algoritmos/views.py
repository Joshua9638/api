from typing import Any, Callable
from django.shortcuts import render, get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import status, viewsets, permissions
from rest_framework import filters, generics
from rest_framework.views import APIView  # vistas
from django.http import Http404
from rest_framework import mixins
from .models import AlgTrayectoriaGyes, AlgTrayectoriaGyeHist, AlgTrayectoriaGyesDetalle  # Modelo
from .serializers import TrayectoriaGyeSerial, TrayectoriaGyeDetalleSerial, TrayectoriaHistSerial  # serializadores
from django.db.models import Prefetch
from django.core.exceptions import ObjectDoesNotExist
from django.http import QueryDict
from rest_framework.views import APIView

from datetime import datetime
import time


##########################################################################
#               TRAYECTORIA GYE - GYE_dETALLE                            #

class AlgoritmoTayectoria(APIView):
    """
       Funcion que permite obtener informacion de una trayectoria
    """
    def get(self, request, format=None):
        origen = self.request.GET.get('origen')
        f = self.request.GET.get('fecha')
        if origen is not None and f is not None:
            try:
                dt = datetime.strptime(f, "%Y-%m-%d %H:%M");
            except:
                return Response('Formato de fecha invalida', status=status.HTTP_400_BAD_REQUEST)
            fecha = dt.strftime("%Y-%m-%d %H:%M")
            trayectoriaGye = AlgTrayectoriaGyes.objects.filter(estado__iexact='A', origen__exact=origen).order_by('id')
            queryset = AlgTrayectoriaGyesDetalle.objects.filter(fecha__year=dt.year, fecha__month=dt.month,
                                                                fecha__day=dt.day, fecha__hour=dt.hour,
                                                                fecha__minute=dt.minute,
                                                                id_trayectoria__in=trayectoriaGye).order_by('id')
            if queryset is not None:
                serializer = TrayectoriaGyeDetalleSerial(queryset, many=True)
                return Response(serializer.data)
            else:
                return Response('No hay datos')
        else:
            return Response('Ingrese los parametros', status=status.HTTP_400_BAD_REQUEST)


trayectoria = AlgoritmoTayectoria.as_view()


##########################################################################
#           TRAYECTORIA HISTORICA                                        #
class AlgTrayectoriaHist(generics.ListAPIView):
    """
        Funcion que permite obtener informacion de una trayectoria historica
    """
    queryset = AlgTrayectoriaGyeHist.objects.all()
    serializer_class = TrayectoriaHistSerial
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('usuario',)
    search_fields = ('usuario',)
    ordering_fields = ('usuario',)
    ordering = ('usuario')


trayectoriaHist = AlgTrayectoriaHist.as_view()


#--------- apis con autenticacion

##########################################################################
#               TRAYECTORIA GYE - GYE_dETALLE                            #

class Algoritmo_Trayectoria(APIView):
    """
        Funcion que permite obtener informacion de una trayectoria
    """
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, format=None):
        origen = self.request.GET.get('origen')
        f = self.request.GET.get('fecha')
        if origen is not None and f is not None:
            try:
                dt = datetime.strptime(f, "%Y-%m-%d %H:%M");
            except:
                return Response('Formato de fecha invalida', status=status.HTTP_400_BAD_REQUEST)
            fecha = dt.strftime("%Y-%m-%d %H:%M")
            trayectoriaGye = AlgTrayectoriaGyes.objects.filter(estado__iexact='A', origen__exact=origen).order_by('id')
            queryset = AlgTrayectoriaGyesDetalle.objects.filter(fecha__year=dt.year, fecha__month=dt.month,
                                                                fecha__day=dt.day, fecha__hour=dt.hour,
                                                                fecha__minute=dt.minute,
                                                                id_trayectoria__in=trayectoriaGye).order_by('id')
            if queryset is not None:
                serializer = TrayectoriaGyeDetalleSerial(queryset, many=True)
                return Response(serializer.data)
            else:
                return Response('No hay datos')
        else:
            return Response('Ingrese los parametros', status=status.HTTP_400_BAD_REQUEST)


trayectoria2 = Algoritmo_Trayectoria.as_view()


##########################################################################
#           TRAYECTORIA HISTORICA                                        #
class Alg_Trayectoria_Hist(generics.ListAPIView):
    """
        Funcion que permite obtener informacion de una trayectoria historica
    """
    permission_classes = (permissions.IsAuthenticated,)
    queryset = AlgTrayectoriaGyeHist.objects.all()
    serializer_class = TrayectoriaHistSerial
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('usuario',)
    search_fields = ('usuario',)
    ordering_fields = ('usuario',)
    ordering = ('usuario')


trayectoriaHist2 = Alg_Trayectoria_Hist.as_view()
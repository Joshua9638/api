"""Apis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.contrib import admin
from django.urls import path
#from apps.ConteoVehiculos.views import Usuario
from apps.apis import views
from django.conf.urls import include, url
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Lesstraffic APIS REST')
urlpatterns = [
    path('admin/', admin.site.urls),
    #path('', include('apps.ConteoVehicular.urls')),
    url(r'^Apis/', include('apps.ConteoVehicular.urls')),
    url(r'^Apis/', include('apps.PuntosGeoreferenciales.urls')),
    url(r'^Apis/', include('apps.ReplicacionesRecoleccion.urls')),
    url(r'^Apis/', include('apps.Indicadores.urls')),
    #url(r'^Apis/', include('apps.Algoritmos.urls')),
    url(r'^Apis/', include('apps.Sectores.urls')),
    url(r'^Apis/', include('apps.RedesSociales.urls')),
    url(r'^Apis/', include('apps.ServicioSecuencias.urls')),
    url(r'^Apis/', include('apps.Algoritmos.urls')),
    url(r'^Apis/login/$', obtain_auth_token),
    url(r'^Apis/registrarUsuarioApis/$',views.registrarUsuarioApis),
    url(r'^Apis/docs/$',schema_view),
]

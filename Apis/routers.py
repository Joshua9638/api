

class RutasGpsRouter(object):

    def db_for_read(self, model, **hints):

        if model._meta.app_label == 'rutasGPS':
            return 'rutas_gps'
        return None

    def db_for_write(self, model, **hints):

        if model._meta.app_label == 'rutasGPS':
            return 'rutas_gps'
        return None

    def allow_relation(self, obj1, obj2, **hints):

        if obj1._meta.app_label == 'rutasGPS' or \
            obj2._meta.app_label == 'rutasGPS':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):

        if app_label == 'rutasGPS':
            return db == 'rutas_gps'
        return None


class DatosGyeRouter(object):

    def db_for_read(self, model, **hints):

        if model._meta.app_label == 'datosGye':
            return 'datos_gye'
        return None

    def db_for_write(self, model, **hints):

        if model._meta.app_label == 'datosGye':
            return 'datos_gye'
        return None

    def allow_relation(self, obj1, obj2, **hints):

        if obj1._meta.app_label == 'datosGye' or \
                obj2._meta.app_label == 'datosGye':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):

        if app_label == 'datosGye':
            return db == 'datos_gye'
        return None